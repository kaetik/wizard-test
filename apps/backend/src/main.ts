import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { AddMigrations } from 'config';
// import { SeedType } from 'config/interfaces';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = await app.get(ConfigService);
  const migrations = await app.get(AddMigrations);

  app.enableCors({
    origin: config.get<string>('allowedOrigin', '').split(';')
  });

  app.setGlobalPrefix('api');

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Wizard v1')
    .setDescription('Wizard API')
    .setVersion('1.0')
    .addTag('Population')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api/docs', app, document);

  const port = config.get<number>('API_PORT');

  await app.listen(port || 3040, () => {
    console.log('App started', port);
  });

  migrations.createInitTables();

  // Seed tables
  // await migrations.seedCitiesFakeData(SeedType.CITIE, 50);
  // await migrations.seedCitiesFakeData(SeedType.RESIDENTS, 1000000);
}
bootstrap();
