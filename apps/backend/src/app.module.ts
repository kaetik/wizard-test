import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PopulationModule } from './population/population.module';
import { DatabaseModule, MigrationsModule, AddMigrations } from '../config';
import { LoggerModule } from './logger/logger.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env' }),
    DatabaseModule,
    LoggerModule,
    MigrationsModule,
    PopulationModule
  ],
  providers: [AddMigrations]
})
export class AppModule {}
