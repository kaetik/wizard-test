import { Request, Response } from 'express';
import axios from 'axios';

const logRequest = (req: Request, res: Response): void => {
  const start = Date.now();

  res.on('finish', () => {
    const duration = Date.now() - start;
    const requestData = {
      method: req.method,
      originalUrl: req.originalUrl,
      body: req.body
    };
    // get some data from response
    console.log(duration);
    const responseData = res.get('Content-Length');
    const statusCode = res.statusCode;

    const logData = {
      duration,
      requestData,
      responseData,
      statusCode
    };

    axios.post('http://localhost:8765/logger', logData)
      .catch((error) => {
        console.log(`Erorr: logger request failed: ${error}`);
      });
  });
};

export { logRequest };
