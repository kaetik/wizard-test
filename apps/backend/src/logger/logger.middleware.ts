import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

import { logRequest } from './request.logger';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  public use(req: Request, res: Response, next: () => void): void {
    logRequest(req, res);
    next();
  }
}
