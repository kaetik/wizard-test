export interface CityPopulation {
  city: string;
  count: number;
}

export interface CityMembers {
  first_name: string;
  count: number;
}

export interface MembersByCity {
  city: string;
  members: CityMembers[];
}

export interface CitiesPopolation {
  cities_population: CityPopulation[];
}

export interface CitiesMembers {
  city_members: MembersByCity[];
}
