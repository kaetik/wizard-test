import { Module } from '@nestjs/common';
import { PopulationService } from './domain/services';
import { PopulationController } from './application/controllers';
import { DatabaseModule } from 'config';

@Module({
  imports: [DatabaseModule],
  controllers: [PopulationController],
  providers: [PopulationService]
})
export class PopulationModule {}
