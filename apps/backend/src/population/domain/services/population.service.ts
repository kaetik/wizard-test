import { Inject, Injectable } from '@nestjs/common';
import { PG_CONNECTION } from 'config';
import {
  CitiesMembers,
  CitiesPopolation,
  CityPopulation,
  MembersByCity
} from '../../infrastructure/interfaces';

@Injectable()
export class PopulationService {
  constructor(@Inject(PG_CONNECTION) private dbConnection: any) {}

  /**
   *
   * @param city filter rparam
   * @returns CitiesPopolation
   */
  public async getCitiesPopulation(city: string): Promise<CitiesPopolation> {
    try {
      const where: string = !!city ? `WHERE c.name ILIKE '${city}%'` : '';

      const cityPopulation: { rows: CityPopulation[] } = await this.dbConnection
        .query(`
        SELECT c.name AS city, COUNT(r.id) AS count
        FROM cities c
        LEFT JOIN residents r ON c.id = r.city_id
        ${where}
        GROUP BY c.id
        ORDER BY count DESC;
      `);

      return {
        cities_population: cityPopulation.rows
      };
    } catch (error) {
      console.log(`Error: getting cities population: ${error}`);
    }
  }

  /**
   *
   * @param city filter rparam
   * @returns CitiesMembers
   */
  public async getCitiesPopulationByFirstName(
    city: string
  ): Promise<CitiesMembers> {
    try {
      // I tried "like" with 250 cities and 1 million residents and it works pretty well
      // but performance could be improved by full-text search if need
      const where: string = !!city ? `WHERE c.name ILIKE '${city}%'` : '';

      /**
       * Could be used requesl like this one and handle result by loop data:
       * SELECT c.name AS city_name, r.first_name, COUNT(r.first_name) AS qty_of_people
       * FROM residents r
       * JOIN cities c ON r.city_id = c.id
       * GROUP BY c.name, r.first_name
       */
      const citiesPopulationsQuery: { rows: MembersByCity[] } = await this.dbConnection.query(`
        SELECT
            subquery.city AS city,
            json_agg(json_build_object('first_name', first_name, 'count', count)) AS members
        FROM (
            SELECT c.id AS cId, c.name AS city, r.first_name, COUNT(r.first_name) AS count
            FROM cities c
            LEFT JOIN residents r ON c.id = r.city_id
            ${where}
            GROUP BY cId, r.first_name
        ) AS subquery
        GROUP BY subquery.city, subquery.cId
        ORDER BY subquery.city, max(subquery.count) DESC;
      `);

      return {
        city_members: citiesPopulationsQuery.rows
      };
    } catch (error) {
      console.log(`Error: getting cities population by first name: ${error}`);
    }
  }
}
