import { Controller, Get, HttpCode, HttpStatus, Param } from '@nestjs/common';
import { ApiOkResponse, ApiParam, ApiTags } from '@nestjs/swagger';
import { PopulationService } from 'src/population/domain/services/population.service';
import { CitiesMembers, CitiesPopolation } from 'src/population/infrastructure/interfaces';

@ApiTags('Population')
@Controller('population')
export class PopulationController {
  constructor(private readonly populationService: PopulationService) {}

  @ApiOkResponse({
    description: 'Get people with the same names'
  })
  @ApiParam({
    name: 'city',
    required: false,
    description: 'City',
    type: 'string'
  })
  @HttpCode(HttpStatus.OK)
  @Get('/same_first_names/:city?')
  public async getPopulationByFirstName(
    @Param('city') city: string
  ): Promise<CitiesMembers> {
    return await this.populationService.getCitiesPopulationByFirstName(city);
  }

  @ApiOkResponse({
    description: 'Get pupolation sorted by cities'
  })
  @ApiParam({
    name: 'city',
    required: false,
    description: 'City',
    type: 'string'
  })
  @HttpCode(HttpStatus.OK)
  @Get('/:city?')
  public async getCitiesPopulation(
    @Param('city') city: string
  ): Promise<CitiesPopolation> {
    return await this.populationService.getCitiesPopulation(city);
  }
}
