import { Inject, Injectable, Module } from '@nestjs/common';
import { PG_CONNECTION } from '../constants';
import { DatabaseModule } from './database';
import { SeedType } from 'config/interfaces';
import { faker } from '@faker-js/faker';

@Injectable()
export class AddMigrations {
  constructor(@Inject(PG_CONNECTION) private dbConnection: any) {}

  public async createInitTables(): Promise<void> {
    try {
      await this.dbConnection.query(`
        CREATE TABLE IF NOT EXISTS migrations (
          id serial PRIMARY KEY,
          migration VARCHAR ( 50 ) UNIQUE NOT NULL,
          timestamp TIMESTAMP NOT NULL
        );
      `);
      const isCitiesTableExists = await this.dbConnection.query(`
        SELECT EXISTS (
          SELECT 1
          FROM information_schema.tables
          WHERE table_name = 'cities'
        ) AS table_existence;
      `);
      if (!isCitiesTableExists.rows[0].table_existence) {
        await this.dbConnection.query(`
        CREATE TABLE "cities" (
          "id" SERIAL NOT NULL,
          "name" character varying NOT NULL,
          "description" character varying NULL,
          CONSTRAINT "PK_589871db156cc7f92942334ab7e" PRIMARY KEY ("id")
        );

        CREATE INDEX idx_cities_name ON cities (name);
      `);
      }

      // If residents table is not exists create new table residents
      const isResidentsTableExists = await this.dbConnection.query(`
        SELECT EXISTS (
          SELECT 1
          FROM information_schema.tables
          WHERE table_name = 'residents'
        ) AS table_existence;
      `);
      if (!isResidentsTableExists.rows[0].table_existence) {
        await this.dbConnection.query(`
          CREATE TABLE "residents" (
            "id" SERIAL NOT NULL,
            "first_name" character varying NOT NULL,
            "last_name" character varying NOT NULL,
            "city_id" integer NOT NULL,
            CONSTRAINT "PK_589871db156cc7f929424ab7e" PRIMARY KEY ("id")
            );

          CREATE INDEX idx_residents_city_id ON residents (city_id);
          CREATE INDEX idx_first_name ON residents (first_name);

          ALTER TABLE "residents" ADD CONSTRAINT "FK_45d515503b0253f6443a4a97cf8" FOREIGN KEY ("city_id") REFERENCES "cities"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
        `);
      }
    } catch (err) {
      console.log(`Error: init tables, ${err}`);
    }
  }

  public async query(
    query: string,
    params?: string[],
    callback?
  ): Promise<void> {
    try {
      await this.dbConnection.query(query, params, callback);
    } catch (err) {
      console.log(`Error: Migration failed, ${err}`);
    }
  }

  public async seedCitiesFakeData(
    type: SeedType,
    amount: number
  ): Promise<void> {
    const maxCityIdQuery = await this.dbConnection.query(
      'SELECT MAX(ID) AS latest FROM cities'
    );
    const citiesCurrentID = maxCityIdQuery.rows[0].latest;
    if (type === SeedType.CITIE) {
      // Generate cities data
      for (let id = citiesCurrentID + 1; id <= citiesCurrentID + amount; id++) {
        this.query(
          'INSERT INTO cities (id, name, description) VALUES ($1, $2, $3)',
          [id, faker.location.city(), faker.lorem.sentence()]
        );
      }
    } else if (type === SeedType.RESIDENTS) {
      const maxResidentsIdQuery = await this.dbConnection.query(
        'SELECT MAX(ID) AS latest FROM residents'
      );
      const residentCurrentID = maxResidentsIdQuery.rows[0].latest;
      for (
        let id = residentCurrentID + 1;
        id <= residentCurrentID + amount;
        id++
      ) {
        this.query(
          'INSERT INTO residents (id, first_name, last_name, city_id) VALUES ($1, $2, $3, $4)',
          [
            id,
            faker.person.firstName(),
            faker.person.lastName(),
            faker.number.int({ min: 1, max: citiesCurrentID })
          ]
        );
      }
    }
  }
}

@Module({
  imports: [DatabaseModule],
  providers: [AddMigrations],
  exports: [AddMigrations]
})
export class MigrationsModule {}
