export interface SimpleMigration {
  up(): any;
}

export enum SeedType {
  CITIE = 'cities',
  RESIDENTS = 'residents'
}
