# WIZARS v1



# How to run application



## 1. Execute next commands



* Run next commands to install Node.js with required version:

```bash

nvm  install  18.10.0

nvm  use  18.10.0

```



## 2. Download required apps:

* Install [Docker Desktop for MacOS](https://docs.docker.com/desktop/mac/install/) or [Docker for Ubuntu](https://docs.docker.com/engine/install/ubuntu/) + [Docker linux postinstall step](https://docs.docker.com/engine/install/linux-postinstall/) (Recommended the latest LTS version)



**IGNORE FOR THE COLD START**: Before starting the project please run next commands:

```bash

docker  compose  down  -v && docker  system  prune  -af && docker  volume  prune  -f

```

## 3. How to work with the application locally



## 3.1. Development with Docker Compose



*  **First launch:**

```bash

# Install all packages, need only for the correct work of your IDE

<repo-root-folder>$ npm install



# For the API start up

<repo-root-folder>$ docker compose up --build

```



*  **Frequently used commands:**



```bash

# cleanup Docker (remove containers, volumes, etc)

<repo-root-folder>$ docker compose down -v && docker  system  prune  -af && docker  volume  prune  -f



# cleanup existing node_modules && dist folder

<repo-root-folder>$ find . -type d -name "node_modules" -exec rm -rf "{}"  \; && find  .  -type  d  -name  "dist"  -exec  rm  -rf  "{}"  \;


```
*  **Seed database:**
```bash
# You can edit
/backend/src/main.ts
# Uncomment SeedType import and uncomment strings right after
// Seed tables
# To seed your DB with fake data
# For example
```
```js
await migrations.seedCitiesFakeData(SeedType.RESIDENTS, 1000000)
```
```bash
# Will add 1 million records to Resident table, but first you have to seed cities table
```
## The application usage:
* Application will be available on 3040 port:  [http://localhost:3040](http:localhost:3040)
* Swagger will be available by next address: [http://localhost:3040/api/docs#/](http://localhost:3040/api/docs#/)
* Adminer will be available on 3041 port: [http://localhost:3041/](http://localhost:3041/)